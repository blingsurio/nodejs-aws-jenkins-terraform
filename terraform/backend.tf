terraform {
  backend "s3" {
    bucket = "aws-jenkins-terraform-blingsurio"
    key = "node-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}