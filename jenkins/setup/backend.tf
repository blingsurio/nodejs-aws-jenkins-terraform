terraform {
  backend "s3" {
    bucket = "aws-jenkins-terraform-blingsurio"
    key    = "jenkins.terraform.tfstate"
    region = "ap-northeast-2"
  }
}